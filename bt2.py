
import numpy as np #import thư vien numpy
import matplotlib.pyplot as plt #import thư vien matplotlib
A = .8 #bien do
f = 5 #tan so
t = np.arange(0,1,.01) # tạo 1 list từ 0 đến 1 bước nhảy 0.01
phi = np.pi/4 #gián giá trị phi
x = A*np.cos(2*np.pi*f*t + phi)#gián giá trị biểu thức hàm x
plt.plot(t,x)#vẽ đồ thị x theo t
plt.axis([0,1,-1,1]) #nhận giá trị trên theo danh sách [xmin, xmax, ymin, ymax]
plt.xlabel('time in seconds')#ten trục hoành
plt.ylabel('amplitude')#tên trục tung
plt.show()#hen thị đồ thị
#---------------------------------------------------------------------------------------
##câu2
A = .65#biên độ
fs = 100#tần số
samples = 100
f = 5
phi = 0
n = np.arange(samples) #tạo 1 list từ 0đến 100 bước nhảy 1
T = 1.0/fs # Remember to use 1.0 and not 1!
y = A*np.cos(2*np.pi*f*n*T + phi)#gián gtri ham y
plt.plot(y)
plt.axis([0,100,-1,1])
plt.xlabel('sample index')
plt.ylabel('amplitude')
plt.show()
#----------------------------------------------------------------------------------------
##câu 3
A = .8#bien do
N = 100 # samples
f = 5#tan so
phi = 0
n = np.arange(N)#tạo 1 list từ 0đến 100 bước nhảy 1
y = A*np.cos(2*np.pi*f*n/N + phi)
plt.plot(y)
plt.axis([0,100,-1,1])
plt.xlabel('sample index')
plt.ylabel('amplitude')
plt.show()
#--------------------------------------------------------------------------------------------
#cau4
f = 3
t = np.arange(0,1,.01)# tạo 1 list từ 0 đến 1 bước nhảy 0.01
phi = 0
x = np.exp(1j*(2*np.pi*f*t + phi))#gián giá trị biểu thức hàm x
xim = np.imag(x)#gián giá trị ảo
plt.figure(1)
plt.plot(t,np.real(x))#vẽ đồ thị gtri thực theo t
plt.plot(t,xim)#vẽ đồ thị giá trị ảo theo t
plt.axis([0,1,-1.1,1.1])
plt.xlabel('time in seconds')
plt.ylabel('amplitude')
plt.show()
#----------------------------------------------------------------------------------------------
#câu 5
f = 3
N = 100
fs = 100
n = np.arange(N)#tạo 1 list từ 0đến 100 bước nhảy 1
T = 1.0/fs
t = N*T
phi = 0
x = np.exp(1j*(2*np.pi*f*n*T + phi))#gián giá trị biểu thức hàm x
xim = np.imag(x)#gián giá trị ảo
plt.figure(1)
plt.plot(n*T,np.real(x))#vẽ đồ thị gtri thực theo n*T
plt.plot(n*T,xim)#vẽ đồ thị gtri ảo theo n*T
plt.axis([0,t,-1.1,1.1])
plt.xlabel('t(seconds)')
plt.ylabel('amplitude')
plt.show()
#------------------------------------------------------------------------------------------------
#câu6
f = 3
N = 64
n = np.arange(64)#tạo 1 list từ 0 đến 64 bước nhảy 1
phi = 0
x = np.exp(1j*(2*np.pi*f*n/N + phi))#gián giá trị biểu thức hàm x
xim = np.imag(x)#gián giá trị ảo
plt.figure(1)
plt.plot(n,np.real(x))#vẽ đồ thị gtri thực theo n
plt.plot(n,xim)#vẽ đồ thị gtri ảo theo n
plt.axis([0,samples,-1.1,1.1])
plt.xlabel('sample index')
plt.ylabel('amplitude')
plt.show()
#----------------------------------------------------------------------------------------------
#câu7
N = 44100 # samples
f = 440
fs = 44100
phi = 0
n = np.arange(N)#tạo 1 list từ 0 đến 44100 bước nhảy 1
x = A*np.cos(2*np.pi*f*n/N + phi)#gián giá trị biểu thức hàm x
#scipy.io.wavfile.write(filename, rate, data)[source]
from scipy.io.wavfile import write
write('sine440_1sec.wav', 44100, x)